# TPM - Terminal Password Manager

It's just that. A terminal based password manager.

## For real though
It's a very simple implementation for me to keep practising Rust and figure out how these sorts of things would work so the functionality is limited.

### Build
```bash
cargo build
```

This should build a binary called `tpm` in the `target/debug` dir.

### Commands

For now there are only a few basic commands. None of them take any args (for now as-of 5th May 2024) but will prompt for the master password first to unlock the database.

The database itself consists of `User` objects and `Password` objects. The `User`'s are actual users of the program. When setting up you will create this user with a given name and your password is used to match against this user. `Password` objects are the logins/passwords saved.

#### `init`
Used to initialise the database on first load. If you attempt to call any of the other commands before this then it will run this command anyway.

#### `get`
Will ask for the name of an item to get, returns none and logs an error if there are none found.

#### `add`
Will ask for a name, username and password before adding the entry to the database.

#### `list`
Will log all entries in the database for a given `User`
