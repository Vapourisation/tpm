use crate::lib::crypt;
use crate::lib::db;

use std::path::Path;

use cli_table::{print_stdout, Cell, CellStruct, Style, Table};
use dialoguer::{Confirm, Input, Password};
use hex::encode;
use zeroize::Zeroize;

fn generate_table(vals: Vec<Vec<CellStruct>>, cols: Vec<CellStruct>) {
    let table = vals.table().title(cols).bold(true);

    let _ = print_stdout(table);
}

pub fn add(user_id: &String, password: &String) {
    let name = Input::<String>::new()
        .with_prompt("Please enter a name for the item you'd like to add")
        .interact_text()
        .unwrap();

    let username = Input::<String>::new()
        .with_prompt("Please enter the username of the item you'd like to add")
        .interact_text()
        .unwrap();

    let mut item_pass = Password::new()
        .with_prompt("Enter the item password")
        .with_confirmation("Confirm password", "Passwords did no match. Try again")
        .interact()
        .unwrap();

    let confirmation = Confirm::new()
        .with_prompt(format!(
            "You're about to add item {} with username {}. Are you sure?",
            name, username
        ))
        .interact()
        .unwrap();

    if !confirmation {
        println!("Okay, not adding new item");
        return;
    }

    let password = crypt::encrypt(password, item_pass.clone());

    let pass = encode(password);
    db::Password::add(
        user_id,
        &name.trim().to_string(),
        &username.trim().to_string(),
        &pass,
    )
    .expect("Failed to add password");

    println!("Password {} added", name);
    item_pass.zeroize();
}

pub fn get(password: &String) {
    let name = Input::<String>::new()
        .with_prompt("Please enter the name of the item you'd like to get")
        .interact_text()
        .unwrap();

    let value = db::Password::get_by_name(&name.trim().to_string());

    if value.is_err() {
        error!(
            "Could not find password for item: {:?}. Perhaps you haven't added one yet.",
            name
        );
        return;
    }

    let value = value.unwrap();

    let decrypted = crypt::decrypt(password, &value.data);

    let vals = vec![vec![
        value.name.cell(),
        value.username.cell(),
        String::from_utf8(decrypted).unwrap().cell(),
    ]];

    let cols = vec![
        "Name".cell().bold(true),
        "Username".cell().bold(true),
        "Password".cell().bold(true),
    ];

    generate_table(vals, cols)
}

pub fn list(user_id: &String, user_pass: &String) {
    let passwords = db::Password::list(user_id).expect("Failed to list passwords");

    let mut vals = vec![];

    for password in passwords {
        vals.push(vec![
            password.name.cell(),
            password.username.cell(),
            String::from_utf8(crypt::decrypt(user_pass, &password.data))
                .unwrap()
                .cell(),
        ]);
    }

    let cols = vec![
        "Name".cell().bold(true),
        "Username".cell().bold(true),
        "Password".cell().bold(true),
    ];

    generate_table(vals, cols);
}

pub fn search(user_id: &String, user_pass: &String) {
    let name = Input::<String>::new()
        .with_prompt("Please enter the name of the item you'd like to search for")
        .interact_text()
        .unwrap();

    let passwords =
        db::Password::search(user_id, &name).expect("Failed to find any matching passwords");

    let mut vals = vec![];

    for password in passwords {
        vals.push(vec![
            password.name.cell(),
            password.username.cell(),
            String::from_utf8(crypt::decrypt(user_pass, &password.data))
                .unwrap()
                .cell(),
        ]);
    }

    let cols = vec![
        "Name".cell().bold(true),
        "Username".cell().bold(true),
        "Password".cell().bold(true),
    ];

    generate_table(vals, cols);
}

pub fn init() -> bool {
    let has_user = db::User::exists().expect("No user found.");

    if !has_user {
        info!("User does not exist");
        let mut name = Input::new()
            .with_prompt("Enter a username")
            .interact_text()
            .unwrap();

        let mut user_pass = Password::new()
            .with_prompt("Please enter a master password")
            .interact()
            .unwrap();

        let pass_hash = crypt::hash(&user_pass);
        db::User::add(&name, &pass_hash).expect("Failed to add user");
        close_database_file(&user_pass);
        user_pass.zeroize();
        return true;
    }

    info!("User already exists");

    false
}

// Open and close database
pub fn open_database_file(password: &String) {
    if Path::new("./passwords.enc").exists() {
        crypt::decrypt_large_file("./passwords.enc", "./passwords.db", password)
            .expect("Failed to decrypt database");
    } else {
        info!("[DB] Database file not found. Creating a new one...");
        let _ = db::init();
    }
}

pub fn close_database_file(password: &String) {
    if Path::new("./passwords.db").exists() {
        crypt::encrypt_large_file("./passwords.db", "./passwords.enc", password)
            .expect("Failed to encrypt database");
    } else {
        info!("[DB] Database file not found.");
    }
}
