use rusqlite::{params, types::Null, Connection, Result};
use serde::{Deserialize, Serialize};
use thiserror::Error;
use uuid::Uuid;

#[derive(Error, Debug)]
pub enum Error {
    #[error("error reading the DB file: {0}")]
    ReadDBError(#[from] std::io::Error),
    #[error("error parsing the DB file: {0}")]
    ParseDBError(#[from] serde_json::Error),
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct User {
    pub id: String,
    pub hash: String,
    pub name: String,
}

impl User {
    pub fn add(name: &String, password_hash: &String) -> Result<()> {
        let conn = Connection::open("passwords.db")?;
        conn.execute(
            "INSERT INTO users (id, name, hash) VALUES (?1, ?2, ?3)",
            params![Uuid::new_v4().to_string(), name, password_hash],
        )?;
        Ok(())
    }

    pub fn get() -> Result<User> {
        let conn = Connection::open("passwords.db")?;
        let mut stmt = conn.prepare("SELECT id, hash, name FROM users LIMIT 1")?;
        let row = stmt.query_row([], |row| {
            Ok(User {
                id: row.get(0)?,
                hash: row.get(1)?,
                name: row.get(2)?,
            })
        })?;
        Ok(row)
    }

    pub fn list() -> Result<Vec<User>> {
        let conn = Connection::open("passwords.db")?;
        let mut stmt = conn.prepare("SELECT id, hash, name FROM users")?;
        let rows = stmt.query_map([], |row| {
            Ok(User {
                id: row.get(0)?,
                hash: row.get(1)?,
                name: row.get(2)?,
            })
        })?;
        let mut users = Vec::new();
        for row in rows {
            users.push(row?);
        }
        Ok(users)
    }

    pub fn exists() -> Result<bool> {
        let conn = Connection::open("passwords.db")?;
        let mut stmt = conn.prepare("SELECT id, hash, name FROM users LIMIT 1")?;
        let row = stmt.query_row([], |row| {
            Ok(User {
                id: row.get(0)?,
                hash: row.get(1)?,
                name: row.get(2)?,
            })
        });
        Ok(row.is_ok())
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Password {
    id: String,
    pub name: String,
    pub username: String,
    pub data: String,
    user_id: String,
}

impl Password {
    pub fn add(user_id: &String, name: &String, username: &String, data: &String) -> Result<()> {
        let conn = Connection::open("passwords.db")?;
        conn.execute(
            "INSERT INTO passwords (id, name, username, data, user_id) VALUES (?1, ?2, ?3, ?4, ?5)",
            params![Uuid::new_v4().to_string(), name, username, data, user_id],
        )?;
        Ok(())
    }

    pub fn add_empty(user_id: &String) -> Result<()> {
        let conn = Connection::open("passwords.db")?;
        conn.execute(
            "INSERT INTO passwords (id, name, username, data, user_id) VALUES (?1, ?2, ?3, ?4, ?5)",
            params![Uuid::new_v4().to_string(), &Null, &Null, &Null, user_id],
        )?;
        Ok(())
    }

    pub fn get_by_name(name: &String) -> Result<Password> {
        let conn = Connection::open("passwords.db")?;
        let mut stmt = conn
            .prepare("SELECT id, name, username, data, user_id FROM passwords WHERE name = ?1")?;
        let row = stmt.query_row([name], |row| {
            Ok(Password {
                id: row.get(0)?,
                name: row.get(1)?,
                username: row.get(2)?,
                data: row.get(3)?,
                user_id: row.get(4)?,
            })
        })?;
        Ok(row)
    }

    pub fn list(user_id: &String) -> Result<Vec<Password>> {
        let conn = Connection::open("passwords.db")?;
        let mut stmt = conn.prepare(
            "SELECT id, name, username, data, user_id FROM passwords WHERE user_id = ?1",
        )?;
        let rows = stmt.query_map([user_id], |row| {
            Ok(Password {
                id: row.get(0)?,
                name: row.get(1)?,
                username: row.get(2)?,
                data: row.get(3)?,
                user_id: row.get(4)?,
            })
        })?;
        let mut passwords = Vec::new();
        for row in rows {
            passwords.push(row?);
        }
        Ok(passwords)
    }

    pub fn search(user_id: &String, name: &String) -> Result<Vec<Password>> {
        let conn = Connection::open("passwords.db")?;
        let mut stmt = conn.prepare("SELECT id, name, username, data, user_id FROM passwords WHERE user_id = ?1 AND name LIKE ?2")?;
        let rows = stmt.query_map([user_id, name], |row| {
            Ok(Password {
                id: row.get(0)?,
                name: row.get(1)?,
                username: row.get(2)?,
                data: row.get(3)?,
                user_id: row.get(4)?,
            })
        })?;
        let mut passwords = Vec::new();
        for row in rows {
            passwords.push(row?);
        }
        Ok(passwords)
    }
}

pub fn init() -> Result<()> {
    let conn = Connection::open("passwords.db")?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS passwords (
            id TEXT PRIMARY KEY,
            user_id TEXT NULL,
            name TEXT NULL,
            username TEXT NULL,
            data BLOB NULL
        )",
        [],
    )?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS users (
            id TEXT PRIMARY KEY,
            hash TEXT NULL,
            name TEXT NULL
        )",
        [],
    )?;
    let _ = conn.close();
    Ok(())
}
