use std::fs::{remove_file, File};
use std::io::{Read, Write};

use argon2::{
    password_hash::{PasswordHash, PasswordHasher, PasswordVerifier, SaltString},
    Argon2,
};
use console::style;
use orion::hazardous::{
    aead::xchacha20poly1305::{open, seal, Nonce, SecretKey},
    mac::poly1305::POLY1305_OUTSIZE,
    stream::chacha20::CHACHA_KEYSIZE,
    stream::xchacha20::XCHACHA_NONCESIZE,
};

use orion::kdf::{derive_key, Password, Salt};
use rand_core::{OsRng, RngCore};

const NONCE_PLUS_AD_SIZE: usize = XCHACHA_NONCESIZE + CHACHA_KEYSIZE;

/// Split encrypted cipher text into IV, AD and encrypted text
fn split_encrypted(cipher_text: &[u8]) -> (Vec<u8>, Vec<u8>, Vec<u8>) {
    (
        cipher_text[..XCHACHA_NONCESIZE].to_vec(),
        cipher_text[XCHACHA_NONCESIZE..NONCE_PLUS_AD_SIZE].to_vec(),
        cipher_text[NONCE_PLUS_AD_SIZE..].to_vec(),
    )
}

fn simple_split_encrypted(cipher_text: &[u8]) -> (Vec<u8>, Vec<u8>) {
    (
        cipher_text[..CHACHA_KEYSIZE].to_vec(),
        cipher_text[CHACHA_KEYSIZE..].to_vec(),
    )
}

/// Fill passed array with cryptographically random data from ring crate
fn get_random(dest: &mut [u8]) {
    RngCore::fill_bytes(&mut OsRng, dest);
}

fn nonce() -> Vec<u8> {
    let mut randoms: [u8; 24] = [0; 24];
    get_random(&mut randoms);
    randoms.to_vec()
}

fn auth_tag() -> Vec<u8> {
    let mut randoms: [u8; 32] = [0; 32];
    get_random(&mut randoms);
    randoms.to_vec()
}

// ------------------------------------------------------------------------------------------------------
// Hashing
// ------------------------------------------------------------------------------------------------------

pub fn hash(password: &String) -> String {
    let salt = SaltString::generate(&mut OsRng);

    // Argon2 with default params (Argon2id v19)
    let argon2 = Argon2::default();

    let password_hash = argon2.hash_password(password.as_bytes(), &salt).unwrap();

    password_hash.to_string()
}

pub fn compare_hashes(password: &String, hash: String) -> bool {
    let parsed_hash = PasswordHash::new(&hash).expect("Failed to parse hash");
    return Argon2::default()
        .verify_password(password.as_bytes(), &parsed_hash)
        .is_ok();
}

// ------------------------------------------------------------------------------------------------------
// Encryption Helpers
// ------------------------------------------------------------------------------------------------------

fn create_key(password: &String, nonce: &Vec<u8>) -> SecretKey {
    let password = Password::from_slice(password.as_bytes()).unwrap();
    let salt = Salt::from_slice(nonce.as_slice()).unwrap();
    let kdf_key = derive_key(&password, &salt, 15, 1024, CHACHA_KEYSIZE as u32).unwrap();
    let key = SecretKey::from_slice(kdf_key.unprotected_as_bytes()).unwrap();
    key
}

pub fn encrypt(password: &String, data: String) -> Vec<u8> {
    let nonce = nonce();
    let key = create_key(password, &nonce);
    let nonce = Nonce::from_slice(nonce.as_slice()).unwrap();
    let ad = auth_tag();

    // Get the output length
    let output_len = match data
        .len()
        .checked_add(XCHACHA_NONCESIZE + POLY1305_OUTSIZE + ad.len())
    {
        Some(min_output_len) => min_output_len,
        None => panic!("Plaintext is too long"),
    };

    // Allocate a buffer for the output
    let mut output = vec![0u8; output_len];
    output[..XCHACHA_NONCESIZE].copy_from_slice(nonce.as_ref());
    output[XCHACHA_NONCESIZE..NONCE_PLUS_AD_SIZE].copy_from_slice(ad.as_ref());
    seal(
        &key,
        &nonce,
        data.as_bytes(),
        Some(ad.clone().as_slice()),
        &mut output[NONCE_PLUS_AD_SIZE..],
    )
    .unwrap();
    output
}

pub fn decrypt(password: &String, cipher_text: &String) -> Vec<u8> {
    let ciphertext = hex::decode(cipher_text).unwrap();
    let key = create_key(password, &ciphertext[..XCHACHA_NONCESIZE].to_vec());
    let split = split_encrypted(ciphertext.as_slice());
    let nonce = Nonce::from_slice(split.0.as_slice()).unwrap();
    let mut output = vec![0u8; split.2.len()];

    open(
        &key,
        &nonce,
        split.2.as_slice(),
        Some(split.1.as_slice()),
        &mut output,
    )
    .unwrap();
    // Remove any remaining padding
    output.retain(|&x| x != 0u8);
    output.to_vec()
}

// ------------------------------------------------------------------------------
// File encryption
// ------------------------------------------------------------------------------

fn encrypt_core(dist: &mut File, contents: Vec<u8>, key: &SecretKey, nonce: Nonce) {
    let ad = auth_tag();
    // Get the output length
    let output_len = match contents.len().checked_add(POLY1305_OUTSIZE + ad.len()) {
        Some(min_output_len) => min_output_len,
        None => panic!("Plaintext is too long"),
    };

    // Allocate a buffer for the outp
    let mut output = vec![0u8; output_len];
    output[..CHACHA_KEYSIZE].copy_from_slice(ad.as_ref());
    seal(
        key,
        &nonce,
        contents.as_slice(),
        Some(ad.clone().as_slice()),
        &mut output[CHACHA_KEYSIZE..],
    )
    .unwrap();
    dist.write_all(output.as_slice()).unwrap();
}

fn decrypt_core(dist: &mut File, contents: Vec<u8>, key: &SecretKey, nonce: Nonce) {
    let split = simple_split_encrypted(contents.as_slice());
    let mut output = vec![0u8; split.1.len() - POLY1305_OUTSIZE];

    open(
        key,
        &nonce,
        split.1.as_slice(),
        Some(split.0.as_slice()),
        &mut output,
    )
    .unwrap();
    dist.write_all(output.as_slice()).unwrap();
}

pub fn encrypt_file(
    source_file_path: &str,
    dist_file_path: &str,
    password: &String,
) -> Result<(), orion::errors::UnknownCryptoError> {
    info!("[ENC] Starting encryption...");

    let nonce = nonce();
    let key = create_key(password, &nonce);
    let nonce = Nonce::from_slice(nonce.as_slice()).unwrap();

    let mut source_file = File::open(source_file_path).unwrap();
    let mut dist_file = File::create(dist_file_path).unwrap();

    let mut contents = Vec::new();
    source_file.read_to_end(&mut contents).unwrap();

    encrypt_core(&mut dist_file, contents, &key, nonce);
    remove_file(source_file_path).unwrap();
    Ok(())
}

pub fn decrypt_file(
    encrypted_file_path: &str,
    dist: &str,
    password: &String,
) -> Result<(), orion::errors::UnknownCryptoError> {
    let mut encrypted_file = File::open(encrypted_file_path).unwrap();
    let mut dist_file = File::create(dist).unwrap();

    let mut contents = Vec::new();
    encrypted_file.read_to_end(&mut contents).unwrap();

    let key = create_key(password, &contents[..XCHACHA_NONCESIZE].to_vec());
    let nonce = Nonce::from_slice(contents[..XCHACHA_NONCESIZE].to_vec().as_slice()).unwrap();

    decrypt_core(&mut dist_file, contents, &key, nonce);
    remove_file(encrypted_file_path).unwrap();
    Ok(())
}

const CHUNK_SIZE: usize = 128; // The size of the chunks you wish to split the stream into.

pub fn encrypt_large_file(
    file_path: &str,
    output_path: &str,
    password: &String,
) -> Result<(), orion::errors::UnknownCryptoError> {
    let mut source_file = File::open(file_path).expect("Failed to open input file");
    let mut dist = File::create(output_path).expect("Failed to create output file");

    let mut src = Vec::new();
    source_file
        .read_to_end(&mut src)
        .expect("Failed to read input file");

    let nonce = nonce();

    dist.write_all(nonce.as_slice()).unwrap();
    let key = create_key(password, &nonce);
    let nonce = Nonce::from_slice(nonce.as_slice()).unwrap();

    println!("{} Encrypting...", style("[*]").yellow());

    for src_chunk in src.chunks(CHUNK_SIZE) {
        encrypt_core(&mut dist, src_chunk.to_vec(), &key, nonce);
    }

    println!("{} Finished encrypting", style("[#]").green());

    remove_file(file_path).unwrap();
    Ok(())
}

pub fn decrypt_large_file(
    file_path: &str,
    output_path: &str,
    password: &String,
) -> Result<(), orion::errors::UnknownCryptoError> {
    let mut input_file = File::open(file_path).expect("Failed to open input file");
    let mut output_file = File::create(output_path).expect("Failed to create output file");

    let mut src: Vec<u8> = Vec::new();
    input_file
        .read_to_end(&mut src)
        .expect("Failed to read input file");

    let nonce = src[..XCHACHA_NONCESIZE].to_vec();

    src = src[XCHACHA_NONCESIZE..].to_vec();

    let key = create_key(password, &nonce);
    let nonce = Nonce::from_slice(nonce.as_slice()).unwrap();

    println!("{} Decrypting...", style("[*]").yellow());

    for src_chunk in src.chunks(CHUNK_SIZE + CHACHA_KEYSIZE + POLY1305_OUTSIZE) {
        decrypt_core(&mut output_file, src_chunk.to_vec(), &key, nonce);
    }

    println!("{} Finished decrypting", style("[#]").green());

    remove_file(file_path).unwrap();
    Ok(())
}
