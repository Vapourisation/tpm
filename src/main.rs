mod lib;
mod tui;

#[macro_use]
extern crate log;
extern crate clap;
extern crate core;

use clap::{Arg, ColorChoice, Command};
use dialoguer::Password;
use log::{error, warn};
use std::error::Error;
use zeroize::Zeroize;

fn tui_main() -> Result<(), Box<dyn Error>> {
    tui::init_error_hooks()?;
    let terminal = tui::init_terminal()?;

    let mut app = tui::App::default();
    app.run(terminal)?;

    tui::restore_terminal()?;
    Ok(())
}

fn main() {
    env_logger::init();
    let matches = Command::new("TPM")
        .version("0.0.1")
        .color(ColorChoice::Always)
        .about("A simple command line password manager")
        .arg(
            Arg::new("command")
                .value_name("COMMAND")
                .help("What action to perform. Options are init, add, get, list, search"),
        )
        .get_matches();

    let mut password = Password::new()
        .with_prompt("Enter your master password")
        .interact()
        .unwrap();

    lib::commands::open_database_file(&password);

    let command = matches
        .get_one::<String>("command")
        .expect("No command passed. Quitting.");

    if command == "init" {
        let _ = lib::db::init();
        lib::commands::init();
        return;
    }

    let user_exists = lib::db::User::exists();

    if user_exists.is_err() {
        println!("User does not exist! {:?}", user_exists);
        warn!("No Users exist, init method might not have been called or previously failed. Try again");
        return;
    }

    let user = lib::db::User::get().expect("Failed to check user");
    let is_allowed = lib::crypt::compare_hashes(&password, user.hash);

    if !is_allowed {
        error!("Invalid password");
        return;
    }

    match command.as_str() {
        "add" => lib::commands::add(&user.id, &password),
        "get" => lib::commands::get(&password),
        "list" => lib::commands::list(&user.id, &password),
        "search" => lib::commands::search(&user.id, &password),
        _ => warn!("Unknown command: {}", command),
    }

    lib::commands::close_database_file(&password);
    password.zeroize();
}
